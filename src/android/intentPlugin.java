package sevensky.cordova.plugins;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class IntentPlugin extends CordovaPlugin {


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("startActivity")) {
            String appName = args.getString(0);
            String video = args.getString(1);
            String videoType = args.getString(2);
            this.startActivity(appName,video,videoType, callbackContext);
            return true;
        }
        return false;
    }


    private void startActivity(String appName,String video,String videoType, CallbackContext callbackContext) {
        if (appName != null && appName.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (video.length() > 0 && videoType.length() > 0 ) {
                Uri videoUri = Uri.parse(video);
                intent.setDataAndType(videoUri, videoType);
            }
            intent.setPackage( appName );
            this.cordova.getActivity().startActivity( intent );
            callbackContext.success(appName);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
